package com.example.randomimage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.graphics.drawable.toDrawable
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val listOfImages = intArrayOf(
        R.drawable.image1,
        R.drawable.image2,
        R.drawable.image3,
        R.drawable.image4,
        R.drawable.image5,
        R.drawable.image6,
        R.drawable.image6,
        R.drawable.image7,
        R.drawable.image8,
        R.drawable.image9,
        R.drawable.image10,
        R.drawable.image11
    )
    private lateinit var listOfImageViews: List<ImageView>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        imageView1.setOnClickListener(this)
        imageView2.setOnClickListener(this)
        imageView3.setOnClickListener(this)
        imageView4.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        listOfImageViews = listOf<ImageView>(imageView1, imageView2, imageView3, imageView4)
        randomImageView().setImageResource(randomImage())
    }

    private fun randomImage() = listOfImages[(listOfImages.indices).random()]
    private fun randomImageView() = listOfImageViews[(listOfImageViews.indices).random()]

// ვცდილობდი, რომ ისე შემეცვალა სურათები, რომ არ გამეორებულიყო, მაგრამ არ გამოვიდა:

//    private fun changeImage(index:Int) {
//    შევქმენი იმ სურათების ლისტი, რომლებიც ამჟამად იმიჯვიუებზე ეყენა:
//        val listOfCurrentImages =
//            mutableListOf<Int>(
//                R.drawable.image1,
//                R.drawable.image2,
//                R.drawable.image3,
//                R.drawable.image4
//            )
//    თუ რენდომ სურათი არ იქნებოდა ლისტის წევრი, შეცვლილიყო იმიჯვიუს ბექგრაუნდი და ასევე ამ სურათს დაეკავებინა ლისტში ძველის ადგილი
//        if (randomImage() !in listOfCurrentImages) {
//            listOfCurrentImages[index] = randomImage()
//            randomImageView.setImageResource(randomImage())
//        } else {
//    ხოლო თუ იქნებოდა ლისტის წევრი, თავიდან გაშვებულიყო ფუნქცია, მანამ, სანამ if- არ შესრულდებოდა
//            changeImage(index)
//        }
//    }


}









